const app = () => {
    let deck;
    let playerName = '';
    let dealerCards = [];
    let playerCards = [];

    getNewDeck().then(result => {
        if(result.success) {
            deck = result.deck_id;
        }
        else {
            console.log('something went wrong');
        }
    })

    const root = document.getElementById("root");

    const playerDetailsContainer = document.createElement("div");
    playerDetailsContainer.setAttribute("class", "playerDetailsContainer");
    root.appendChild(playerDetailsContainer);

    const playerNameInput = document.createElement("input");
    playerNameInput.setAttribute("class", "playerNameInput");
    playerNameInput.setAttribute("id", "playerNameInput");
    playerNameInput.setAttribute("placeholder", "Enter name");
    playerNameInput.addEventListener("change", (e) => {
        playerName = e.target.value;
        console.log(playerName);
    })
    playerDetailsContainer.appendChild(playerNameInput);

    

    const startGameButton = document.createElement("button");
    startGameButton.textContent = "START GAME";
    startGameButton.setAttribute("class", "startGameButton");

    startGameButton.addEventListener("click", () => {
        try {
            getCardDraw(deck, 2)
            .then(res => {
                console.log(res);
                dealerCards = [res.cards[0], res.cards[1]];
                dealerCardOneImage.src = res.cards[0].images.png;
               
                dealerCardOne.appendChild(dealerCardOneImage);
                console.log(dealerCards);
            })
        } catch(err) {
            console.log('error: '+err);
        }
        try {
            getCardDraw(deck, 2)
            .then(res => {
                console.log(res);
                playerCards = [res.cards[0], res.cards[1]];
                playerCardOneImage.src = res.cards[0].images.png;
                playerCardTwoImage.src = res.cards[1].images.png;
                
                playerCardOne.appendChild(playerCardOneImage);
                playerCardTwo.appendChild(playerCardTwoImage);
                console.log(playerCards);
            })
        } catch(err) {
            console.log('error: '+err);
        }
        playerNameContainer.textContent = playerName;
        root.appendChild(playerNameContainer);
    })
    playerDetailsContainer.appendChild(startGameButton);


    const cardContainer = document.createElement("div");
    cardContainer.setAttribute("class", "cardContainer");
    root.appendChild(cardContainer);

    const dealerCardContainer = document.createElement("div");
    dealerCardContainer.setAttribute("class", "dealerCardContainer");
    dealerCardContainer.innerHTML = "Dealer ".fontsize(6)
    cardContainer.appendChild(dealerCardContainer);

    const playerCardContainer = document.createElement("div");
    playerCardContainer.setAttribute("class", "playerCardContainer");
    playerCardContainer.innerHTML = "Player ".fontsize(6)
    cardContainer.appendChild(playerCardContainer);

    const playerNameContainer = document.createElement("div");


    // Draw card
    const drawCardButton = document.createElement("button");
    drawCardButton.setAttribute("class", "drawCardButton");
    drawCardButton.textContent = "Draw card";
    drawCardButton.addEventListener("click", () => {
        try {
            getCardDraw(deck, 1)
            .then(res => {
                switch(playerCards.length) {
                    case 2:
                        playerCardThreeImage.src = res.cards[0].images.png;
                        playerCardThree.appendChild(playerCardThreeImage);
                        break;
                    case 3:
                        playerCardFourImage.src = res.cards[0].images.png;
                        playerCardFour.appendChild(playerCardFourImage);
                        break;
                    default:
                        console.log('already four cards');
                        break;
                }
                playerCards.push(res.cards[0]);
            })
        } catch(err) {
            console.log('error: '+err);
        }
        
    })
    root.appendChild(drawCardButton);

    const standButton = document.createElement("button");
    standButton.textContent = "Stand!";
    standButton.setAttribute("class", "standButton");
    standButton.addEventListener("click", () => {
        getCardDraw(deck, 1)
        .then(res => {

            dealerCardTwoImage.src = res.cards[0].images.png;
            dealerCardTwo.appendChild(dealerCardTwoImage);
        })
        
    })
    root.appendChild(standButton);

    const dealerCardOne = document.createElement("div");
    dealerCardOne.setAttribute("class", "dealerCard");
    dealerCardContainer.appendChild(dealerCardOne);

    const dealerCardOneImage = document.createElement("img");
    dealerCardOneImage.height = 300;
    dealerCardOneImage.width = 150;

    const dealerCardTwo = document.createElement("div");
    dealerCardTwo.setAttribute("class", "dealerCard");
    dealerCardContainer.appendChild(dealerCardTwo);

    const dealerCardTwoImage = document.createElement("img");
    dealerCardTwoImage.height = 300;
    dealerCardTwoImage.width = 150;

    const playerCardOne = document.createElement("div");
    playerCardOne.setAttribute("class", "playerCard");
    playerCardContainer.appendChild(playerCardOne);

    const playerCardOneImage = document.createElement("img");
    playerCardOneImage.height = 300;
    playerCardOneImage.width = 150;

    const playerCardTwo = document.createElement("div");
    playerCardTwo.setAttribute("class", "playerCard");
    playerCardContainer.appendChild(playerCardTwo);

    const playerCardTwoImage = document.createElement("img");
    playerCardTwoImage.height = 300;
    playerCardTwoImage.width = 150;

    const playerCardThree = document.createElement("div");
    playerCardThree.setAttribute("class", "playerCard");
    playerCardContainer.appendChild(playerCardThree);

    const playerCardThreeImage = document.createElement("img");
    playerCardThreeImage.height = 300;
    playerCardThreeImage.width = 150;

    const playerCardFour = document.createElement("div");
    playerCardFour.setAttribute("class", "playerCard");
    playerCardContainer.appendChild(playerCardFour);

    const playerCardFourImage = document.createElement("img");
    playerCardFourImage.height = 300;
    playerCardFourImage.width = 150;

    
    

}

const getCardDraw = (deck_id, count) => {
    return fetch(`https://www.deckofcardsapi.com/api/deck/${deck_id}/draw/?count=${count}`)
    .then(response => response.json())
}

const getNewDeck = () => {
    return fetch('https://www.deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1')
    .then(response => response.json());
}

app();